#!/bin/bash

echo P====================================================

#number of parameters
	numPar=$(cat parameters | grep = | wc -l)
	echo "The parameters file has $numPar parameters"

#declare and populate arrays
	varNames=($(cat parameters | grep = | sed 's:=.*::'))
	
	valueLines="$(echo "$(cat parameters | grep = | sed 's:.*=::')")"
	declare -a varValues
	IFS=$'\n'
	for item in $valueLines
		do
		varValues+=("$item")
	done
	unset IFS
	
#report variables
	for (( i=0; i<$numPar; i++ ))
		do
		printf "  ${varNames[$i]}:::::::::${varValues[$i]}\n"
	done

function eval {
	name=$1
	flag=""
	index=0
	#get index for name from varNames
	for i in "${!varNames[@]}"
		do
		if [ "${varNames[$i]}" == "${name}" ]
			then
			index="${i}"
			flag="found"
		fi
	done
	#error if nothing found
	if [ "$flag" != "found" ]
		then
		echo "Not able to evaluate the variable: $name"
		exit 1
	fi
	return $index
}

echo V====================================================

X=$(eval X; echo ${varValues[$?]})

steps=$(eval steps; echo ${varValues[$?]})
newLog=$(eval newLog; echo ${varValues[$?]})
export pipeName=$(eval pipeName; echo ${varValues[$?]})
export refGen=$(eval refGen; echo ${varValues[$?]})
export refType=$(eval refType; echo ${varValues[$?]})
export refName=$(basename -s ".${refType}"  "${refGen}")
export logNum=0

echo F====================================================

function testExit {
	status=$1
	step=$2

	if [ "${status}" != 0 ]
		then
		echo "There was a problem with step: $step " | tee -a ../logfile_${pipeName}
		exit 1
	else
		echo "Completed step: $step " | tee -a ../logfile_${pipeName}
	fi
}

function callInit {
        cd call
	rm allSteps:*
	touch "allSteps: ${steps}"
        for c in $steps
		do
		skip=$(echo $c | grep '#')
                if [ ! -e "$c.sh" ] && [ "$skip" == "" ]
                        then
                        cp genericCall.sh "$c.sh"
			echo "Created a generic call for step: $c"
                fi
        done
        cd ..
}

function stableDir {
        echo "setup function stableDir to ensure output directories exist and clean"
}

if [ $newLog == "Y" ]; then echo NewLog > logfile_${pipeName}; fi

echo "====================${pipeName}========================="  | tee -a logfile_${pipeName}

callInit
stableDir

cd data
echo "Working from: $(pwd)"  | tee -a ../logfile_${pipeName}

for i in ${steps}
	do
	
	stepName="${i}"
	(( logNum++ ))

	skip=$(echo "${stepName}" | grep '#')
	if [ "${skip}" == "" ]
		then

		#generic step
		echo "Calling: ${stepName}" | tee -a ../logfile_${pipeName}
		../call/${stepName}.sh
		testExit $? ${stepName}

	else
		echo "SKIP STEP: ${stepName}" | tee -a ../logfile_${pipeName}
	fi
done

echo =========================================================
echo "The ${pipeName} pipeline has completed" | tee -a ../logfile_${pipeName}
