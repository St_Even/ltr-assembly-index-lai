#!/bin/bash

#$ -S /bin/bash
#$ -N runMario_golden_promisev1
#$ -pe smp 48
#$ -cwd
#$ -m ea
#$ -M your.email@gmail.com
#$ -w e

conda activate LTR

./Mario.sh



#call with:$ qsub runMario.sh 
