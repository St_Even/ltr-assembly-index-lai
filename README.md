# LAI PIPELINE DOCUMENTATION

## Introduction

To calculate LAI we applied the MARIO (2) pipeline framework to coordinate processing steps following the protocol as developed by by Shujun Ou et al. (1). The pipeline proceeds through the steps: gt suffixerator (3), gt ltrharvest (4), LTR_FINDER_parallel (5) , concatenate, LTR_retriever (6), RepeatMasker (7), and finally LAI (8). LTR_FINDER_parallel parallelizes calls to ltr_finder (11) with parameters consistent with the protocol. This pipeline and its Conda environment as implemented are available on bitbucket (9). The source code (10) for LTR_FINDER_parallel is not available from Anaconda Cloud and must be manually added to the conda environment bin directory.  

## Pipeline Structure  
![file passing](https://bitbucket.org/St_Even/ltr-assembly-index-lai/downloads/img_1197.jpg)

## Setup  

### Pull this distribution from bitbucket
cd to the install location and run
	
	git clone https://bitbucket.org/St_Even/ltr-assembly-index-lai.git

### Setup Conda environment  
With conda [installed](https://docs.anaconda.com/anaconda/install/linux/)
cd into the cloned directory and run

	conda env create -f environment_LTR.yml

Creating the bioconda environment with the exception of LTR_FINDER_parallel

### Modify Conda environment  

open a terminal and navigate to the conda LTR environment bin directory. On my machine this looks like  
        
        $ cd ~/miniconda3/envs/LTR/bin  
        
create and open a file to recieve the script.  
        
        $ touch LTR_FINDER_parallel  
        $ nano LTR_FINDER_parallel  

Navigate your browser to https://github.com/oushujun/LTR_FINDER_parallel  

Note the bin directory, we will return to this to install the cut.pl script  
Click LTR_FINDER_parallel to open the code  
Highlight and copy the entire perl script, lines 1-244  

paste the script in the file you created, `right click`, exit, `CTL x` and Save  

Create a dir and file to recieve the cut.pl script 
        
        $ mkdir bin  
        $ cd bin  
        $ touch cut.pl  
        $ nano cut.pl  
        
navigate your browser to the cut.pl script, copy lines 1-62, paste, paste and save as before.  

### Modify parameters file  and run Mario.sh

set the ` pipeName refGen refType` parameters to your environment and requirements

the Mario.sh script reads the parameters `steps` to coordinate the pipeline calls and produce documentation of run state. As written the calls are set for 48 cores. A Sungrid qsub script is included, edit the -N name field, and -M email address.


## Step documentation  

========

### gt suffixerator

usage source  
as discussed on the  LTR_retriever gitHub https://github.com/oushujun/LTR_retriever  

man page  
http://manpages.ubuntu.com/manpages/trusty/man1/gt-suffixerator.1.html  

========

### gt ltrharvest  

usage source   
per protocol  

man page  
http://genometools.org/documents/ltrharvest.pdf  

========  

### LTR_FINDER_parallel  

usage source  
per discussion on LTR_retriever github  

man page  
https://github.com/oushujun/LTR_FINDER_parallel  
 
parallelizes calling ltr_finder(c) with parameters per protocol  

	ltr_finder 
         -w 2 -C -D 15000 -d 1000 -L 7000 -l 100 -p 20 -M 0.85  

========  

### Concatenate  

usage source  
per discussion on LTR_retriever man page  

========

### LTR_retriever  

usage source  
per protocol  

man page  
https://github.com/oushujun/LTR_retriever  

========  

### RepeatMasker  

usage source  
per protocol  

man page  
http://www.repeatmasker.org/  
https://www.animalgenome.org/bioinfo/resources/manuals/RepeatMasker.html  

========  

### LAI  

usage source  
per protocol  

man page  
https://github.com/oushujun/LTR_retriever  

========  

## LAI PROTOCOL  
extract from  

### Assessing genome assembly quality using the LTR Assembly Index (LAI)  

> #### Calculation of LTR Assembly Index (LAI)  
>There are four steps to calculate LAI for a genome assembly: (i) obtain LTR retrotransposon candidates; (ii) retain all intact LTR-RTs by filtering out false candidates; (iii) whole-genome LTR-RT annotation; (iv) calculate LAI. In this study, LTR-RT candidates were obtained using LTRharvest (11) with parameters ‘-minlenltr 100 -maxlenltr 7000 -mintsd 4 -maxtsd 6 -motif TGCA -motifmis 1 -similar 85 -vic 10 -seed 20 -seqids yes’ and LTR_FINDER (10) with parameters ‘-D 15000 -d 1000 -L 7000 -l 100 -p 20 -C -M 0.85’. Both of these parameter sets were requiring minimum and maximum LTR length of 100 bp and 7 Kb, respectively, with at least 85% identity between two LTR regions of a candidate. High-confidence LTR retrotransposons with perfect micro-structures of terminal motifs and target site duplication (the ‘pass’ category) were identified from LTR-RT candidates using LTR_retriever (4) with default parameters, which were regarded as intact LTR retrotransposons. All possible LTR sequences in a given genome were an>notated by RepeatMasker using the non-redundant LTR-RT library constructed by LTR_retriever and with parameters ‘-e ncbi -q -no_is -norna -nolow -div 40 -cutoff 225’ (Supplementary Figure S2). Estimation of raw LAI was performed using the equation Raw LAI = (Intact LTR element length / Total LTR sequence length) * 100, which was carried out by the LAI program deployed in the LTR_retriever package with window size set to 3 Mb and sliding step set to 300 Kb (‘-window 3000000 -step 300000’). The whole-genome raw LAI score is also generated in this procedure.  

>Since the raw LAI score is correlated with the activities of LTR-RT (see Results), such as LTR-RT amplification and removal, the mean identity of LTR sequences of the monoploid (1×) genome was used to correct these effects. To estimate the mean identity of LTRs, genomic sequences annotated as LTR regions were extracted and subjected to all-versus-all BLAST. The identity of each sequence hit that has the highest query coverage (except self-alignment) was used to estimate the whole-genome LTR identity. The correction factor of 2.8138 estimated using 20 high-quality long-read genomes was used to correct raw LAI scores with the equation LAI = raw LAI + 2.8138 × (94 – whole genome LTR identity). The LAI is set to 0 when raw LAI = 0 or the adjustment produces a negative value. Estimation of LTR identity and correction of raw LAI were also carried out by the LAI program. The mean age of intact LTR-RTs estimated by LTR_retriever was also used as an indicator of LTR-RT activity, but the age could be overestimated in >draft genomes since young LTR-RTs are among the most poorly assembled. Although LAI is independent of total LTR-RT content, estimation of LAI is empirically not accurate when total LTR-RT content is less than 5% and intact LTR-RT content is less than 0.1%. To control for abnormally high LAI score, the regional LAI is down-scaled to 10% of the original score when total LTR-RT content is less than 1% in both whole-genome and regional LAI estimations. LAI is a default output of LTR_retriever since version 1.5 and freely available through GitHub under the GNU General Public License v3.0 (https://github.com/oushujun/LTR_retriever).  

Shujun Ou et al. (a)  
https://doi.org/10.1093/nar/gky730  

## REFERENCES  

cit (1)  
Shujun Ou, Jinfeng Chen, Ning Jiang, Assessing genome assembly quality using the LTR Assembly Index (LAI), Nucleic Acids 
Research, Volume 46, Issue 21, 30 November 2018, Page e126, https://doi.org/10.1093/nar/gky730  

link (2)  
https://bitbucket.org/St_Even/mario/src/master/  

cit (3)  
G. Gremme, S. Steinbiss and S. Kurtz. 
GenomeTools: a comprehensive software library for efficient processing of structured genome annotations. 
IEEE/ACM Transactions on Computational Biology and Bioinformatics 2013, 10(3):645–656  

cit (4)  
Ellinghaus D., Kurtz S., Willhoeft U. LTRharvest, an efficient and flexible software for de novo detection of LTR retrotransposons. BMC Bioinformatics. 2008; 9:18  

cit (5)  
Ou S, Jiang N. LTR_FINDER_parallel: parallelization of LTR_FINDER enabling rapid identification of long terminal repeat retrotransposons. Mob DNA 2019;10(1):48.  

cit (6)  
Ou S. and Jiang N. (2018). LTR_retriever: A Highly Accurate and Sensitive Program for Identification of Long Terminal Repeat Retrotransposons. Plant Physiol. 176(2): 1410-1422.  

cit (7)  
Smit, AFA, Hubley, R & Green, P. RepeatMasker Open-4.0. 
2013-2015 <http://www.repeatmasker.org>.  

cit (8)  
Ou S., Chen J. and Jiang N. (2018). Assessing genome assembly quality using the LTR Assembly Index (LAI). Nucleic Acids Res. gky730.  

link (9)  
https://bitbucket.org/St_Even/ltr-assembly-index-lai/src/master/  

link (10)  
https://github.com/oushujun/LTR_FINDER_parallel  

cit (11)  
Xu Z., Wang H. LTR_FINDER: an efficient tool for the prediction of full-length LTR retrotransposons. Nucleic Acids Res. 2007; 35:W265–W268.  


## CALLS

        gt suffixerator \
                -tis \
                -suf \
                -lcp \
                -des \
                -ssp \
                -sds \
                -dna \
                -indexname "${refName}".index \
                -db "${refGen}" \
                &> ../log${logNum}_${name}

        gt ltrharvest \
                -minlenltr 100 \
                -maxlenltr 7000 \
                -mintsd 4 \
                -maxtsd 6 \
                -motif TGCA \
                -motifmis 1 \
                -similar 85 \
                -vic 10 \
                -seed 20 \
                -seqids yes \
                -index "${refName}".index \
                > ltrharvest.out \
                2> ../log${logNum}_${name}STDERR

        LTR_FINDER_parallel \
                -seq "${refGen}" \
                -threads 36 \
                -harvest_out \
                &> ../log${logNum}_${name}

        #CAT
                cat ltrharvest.out ${refName}.${refType}.finder.combine.scn > harvest.merged \
                2> ../log${logNum}_${name}STDERR

        LTR_retriever \
                -genome "${refGen}" \
                -inharvest harvest.merged \
                -threads 36 \
                &> ../log${logNum}_${name}
        
        RepeatMasker -pa 9 \
                -e ncbi \
                -q \
                -no_is \
                -norna \
                -nolow \
                -div 40 \
                -cutoff 225 \
                -lib "${refName}.${refType}.LTRlib.fa" \
                "${refGen}" \
                &> ../log${logNum}_${name}

        LAI \
                -genome "${refGen}" \
                -intact "${refName}.${refType}.pass.list" \
                -all "${refName}.${refType}.out" \
                -window 3000000 \
                -step 300000 \
                -t 36 \
                &> ../log${logNum}_${name}  