#!/bin/bash



#make call from data
#set variables
	path=$0
	name=$(basename -s .sh ${path})
	preFiles="$(ls -1)"

function reportProduct {
        postFiles="$(ls -1)"
        newFiles=""

        for f in $postFiles
                do
                oldMatch=$(echo $preFiles | grep "$f")
                if [ "$oldMatch" == "" ]
                        then
                        newFiles="$newFiles $f"
                fi
        done

	#report files created
        printf "\n========\n$name products:\n" | tee -a ../logfile_${pipeName}
        printf " \n$newFiles\n" | tee -a ../logfile_${pipeName}
        printf "\n========\n" | tee -a ../logfile_${pipeName}
}


echo "Beginning: ${path}"

	#print everything between <CALL> <SETTINGS> to main log
	settings=$(cat $path | sed -n '/^ *#<CALL> *$/,/^ *#<SETTINGS> *$/p')
	echo "$settings" >> ../logfile_${pipeName}


<<USAGE
START

Usage: perl LTR_FINDER_parallel -seq [file] -size [int] -threads [int]
Options:        -seq    [file]  Specify the sequence file.
                -size   [int]   Specify the size you want to split the genome sequence.
                                Please make it large enough to avoid spliting too many LTR elements. Default 5000000 (bp)
                -time   [int]   Specify the maximum time to run a subregion (a thread).
                                This helps to skip simple repeat regions that take a substantial of time to run. Default: 1500 (seconds).
                                Suggestion: 300 for -size 1000000. Increase -time when -size increased.
                -try1   [0|1]   If a region requires more time than the specified -time (timeout), decide:
                                        0, discard the entire region.
                                        1, further split to 50 Kb regions to salvage LTR candidates (default);
                -harvest_out    Output LTRharvest format if specified. Default: output LTR_FINDER table format.
                -next           Only summarize the results for previous jobs without rerunning LTR_FINDER (for -v).
                -verbose|-v     Retain LTR_FINDER outputs for each sequence piece.
                -finder [file]  The path to the program LTR_FINDER (default v1.0.7, included in this package).
                -threads|-t     [int]   Indicate how many CPU/threads you want to run LTR_FINDER.
                -check_dependencies Check if dependencies are fullfiled and quit
                -help|-h        Display this help information.

END
USAGE

#<CALL>
     
	LTR_FINDER_parallel \
		-seq "${refGen}" \
		-threads 36 \
                -harvest_out \
		&> ../log${logNum}_${name}

#<SETTINGS>

#test status
	if [ $? -ne 0 ]
		then
		echo "There is a problem in: ${path}" | tee -a ../logfile_${pipeName}
		exit 1
	else
		echo "Completing: ${path}"
		reportProduct
		exit 0
	fi
	
