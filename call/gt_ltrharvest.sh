#!/bin/bash



#make call from data
#set variables
	path=$0
	name=$(basename -s .sh ${path})
	preFiles="$(ls -1)"

function reportProduct {
        postFiles="$(ls -1)"
        newFiles=""

        for f in $postFiles
                do
                oldMatch=$(echo $preFiles | grep "$f")
                if [ "$oldMatch" == "" ]
                        then
                        newFiles="$newFiles $f"
                fi
        done

	#report files created
        printf "\n========\n$name products:\n" | tee -a ../logfile_${pipeName}
        printf " \n$newFiles\n" | tee -a ../logfile_${pipeName}
        printf "\n========\n" | tee -a ../logfile_${pipeName}
}


echo "Beginning: ${path}"

	#print everything between <CALL> <SETTINGS> to main log
	settings=$(cat $path | sed -n '/^ *#<CALL> *$/,/^ *#<SETTINGS> *$/p')
	echo "$settings" >> ../logfile_${pipeName}

#<CALL>
        gt ltrharvest \
                -minlenltr 100 \
                -maxlenltr 7000 \
                -mintsd 4 \
                -maxtsd 6 \
                -motif TGCA \
                -motifmis 1 \
                -similar 85 \
                -vic 10 \
                -seed 20 \
                -seqids yes \
                -index "${refName}".index \
                > ltrharvest.out \
                2> ../log${logNum}_${name}STDERR
#<SETTINGS>

#test status
	if [ $? -ne 0 ]
		then
		echo "There is a problem in: ${path}" | tee -a ../logfile_${pipeName}
		exit 1
	else
		echo "Completing: ${path}"
		reportProduct
		exit 0
	fi
	
